package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class S2A2 {
    public static void main (String[] args) {

        //a
        int[] primeNumbersArray = new int[5];
        primeNumbersArray[0] = 2;
        primeNumbersArray[1] = 3;
        primeNumbersArray[2] = 5;
        primeNumbersArray[3] = 7;
        primeNumbersArray[4] = 11;
        System.out.println("The first prime number is:" + primeNumbersArray[0]);
        System.out.println("The second prime number is:" + primeNumbersArray[1]);
        System.out.println("The third prime number is:" + primeNumbersArray[2]);
        System.out.println("The fourth prime number is:" + primeNumbersArray[3]);
        System.out.println("The fifth prime number is:" + primeNumbersArray[4]);

        //b
        ArrayList<String> teletubbies = new ArrayList<>();
        teletubbies.add("Dipsy");
        teletubbies.add("Tinky-Winky");
        teletubbies.add("Laa-Laa");
        teletubbies.add("Po");
        System.out.println("My friends are: " + teletubbies);

        //c
        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("ham", 200);
        inventory.put("queso de bola", 150);
        inventory.put("pinya", 120);
        System.out.println("Our current inventory consists of: " + inventory);
        System.out.println("Happy New Year po!");
    }
}
