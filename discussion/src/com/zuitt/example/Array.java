package com.zuitt.example;

import java.awt.desktop.SystemEventListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //Java Collection
    //Java collections are a single unit of objects.
    // useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops.
    //list hashmaps topple
    //there a re a lot but these are the concepts that we will use during the course
    public static void main(String[] args){
        //Arrays
        //In java, arrays are container of values of the same type given a predefined amount of values.
        // java arrays are more rigid, once the size and data type are defined, they can no longer be changed.
        //with pre-defined amount of values, we cannot use array methods
        //once we declare, we cannot change its quantity

        //Array Declaration
        // datatype[] identifier = new dataType[numOfElements];
        // "[]" indicates that data type should be able to hold multiple values.
        // "new" keyword is used for non-primitive data types to tell Java to create the said variable.
        // the values of the array is initializes to 0 or null.

        ///non-primitive, objects that we can

        int[] intArray = new int[5];

        //we reassigned our values

        // intArray[0] = 200;
        // intArray[1] = 3;
        // intArray[2] = 25;
        // intArray[3] = 50;
        // intArray[4] = 99;

        // Once we did not assign any value - [0, 0, 0, 0, 0]

        //intArray[5] = 100;
        //Index 5 out of bounds for length 5

        //this will return the memory address of the array
        //System.out.println(intArray);

        //double-click on Array utils
        //To print the intArray, we need to import the Array Class and use the .toString() method.
        //This will convert the array as a string in the terminal.
        //method of the array class - one is toString()
        System.out.println(Arrays.toString(intArray));

        //Syntax: Array Declaration with initialization
            //dataType[] identifier = {elementA, elementB, elementC, ...};
            // the compiler automatically specifies the size by counting the number of elements in the array

        //declare an array of strings
        String[] names = {"John","Jane","Joe"};
        //names[4] = "Joey"; //out of bounds

        System.out.println(Arrays.toString(names));

        //Sample java array method:
        //Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort(): " + Arrays.toString(intArray));

        //Multidimensional arrays
        //A two-dimensional array then, can be described by two lengths nested within each other, like a matrix.
        // first length is row, second length is column

        //common sa data science, matrices

        String[][] classroom = new String[3][3];

        //[row][column]

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //printout ram address
        //System.out.println(Arrays.toString(classroom));

        //works on 2D array only
        //we use the deepToString() for printing two-dimensional array
        System.out.println(Arrays.deepToString(classroom));

        //In Java, the size of the array cannot be modified. If there is a need to add or remove elements, new arrays must be created

        //ArrayLists
        // are resizable arrays, wherein elements can be added or removed whenever it is needed.
        //arrays - items have similar data

        //generic, specify this list will only have items with similar data type

        // ArrayList<T> identifier = new ArrayList<T>();
        // "<T>" is used to specify that the list can only have one type of object in a collection.
        // ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to use this types as object.
        // in short, Object version of primitive data types with methods.

        //declaring an array list
        //cannot hold primitive data types
        //ArrayList<int> numbers = new ArrayList<int>();

        //ArrayList<Integer> numbers = new ArrayList<>();
        //Wrapper classes for data conversion, Integer classes, there are a lot of methods
        //We used Integer here because the value of the Array List should be in a number/integer data type
        //ArrayList<Integer> numbers = new ArrayList<Integer>();
        //valid

        //ArrayList<String> students = new ArrayList<String>();

        //Declaring an ArrayList with Values

        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane","Mike"));

        //add elements
        //arrayListName.add(element);

        students.add("John");
        students.add("Paul");

        System.out.println(students); //[Jane, Mike, John, Paul]

        //Access element
        //arrayListName.get(index)

        System.out.println(students.get(0)); //Jane
        //System.out.println(students.get(5));//exception//out of bounds//Index 5 out of bounds for length 4

        //Add an element on a specific index
        //ArrayListName.add(index, element);
        //do not type index and element, once you type 0, automatic
        students.add(0  ,"Joey");
        System.out.println(students); //[Joey, Jane, Mike, John, Paul]

        //Updating an element
        //arrayListName.set(index,element);
        students.set(0,"George");
        System.out.println(students); //[George, Jane, Mike, John, Paul]

        //Removing a specific element
        //arrayListName.remove(index);
        students.remove(1);
        System.out.println(students); //[George, Mike, John, Paul]

        //Removing all elements
        students.clear();
        System.out.println(students); //[]

        //getting the arrayList size
        System.out.println(students.size()); //0

        //Hashmaps
        // most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods.
        // There are might be use cases where is this not appropriate, or you may simply want to store a collection of data in key-value pairs
        //CAN BE compared to local storage
        // in Java "keys" also referred as "fields"
        // wherein the values are accessed by the fields
        // Syntax:
        // HashMap<dataTypeField, dataTypeValue>
        // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();
        //we label it with a specific field, key or name instead of index

        //Declaring HashMaps
        //Right click and then show content actions
        //HashMap<String, String> jobPosition = new HashMap<String, String>();

        //Declaring HashMaps with Initialization
        HashMap<String, String> jobPosition = new HashMap<String, String>(){
            {
                put("Teacher","John");
                put("Artist","Jane");
            }
        };

        //Add elements
        //hashMapName.put(<fieldName>, <value>);
        jobPosition.put("Student","Brandon");
        jobPosition.put("Dreamer","Alice");
        //jobPosition.put("Student","Jane"); //{Student=Jane, Dreamer=Alice}
        //the last one will be overridden
        System.out.println(jobPosition);

        //Accessing elements
        //hasMapName.get("fieldName");
        System.out.println(jobPosition.get("Student"));
        //System.out.println(jobPosition.get("student")); //yields a null value, case-sensitivity//null
        //System.out.println(jobPosition.get("Admin"));//null//may hinanap pero wala aww//non-existing field will result to null

        //Updating the values
        //hashMapName.replace("fieldNameToChange","newValue");
        jobPosition.replace("Student","Brandon Smith");//{Artist=Jane, Teacher=John, Student=Brandon Smith, Dreamer=Alice}
        //we cannot change the field name
        System.out.println(jobPosition);

        // Remove an element
        //hashMapName.remove(key);
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        //Retrieve HashMap Keys
        //hashMapName.keySet();
        System.out.println(jobPosition.keySet());//return as array
        //[Artist, Teacher, Student]

        //Retrieve HashMap Values
        //hashMapName.values();
        System.out.println(jobPosition.values());//return as an array
        //[Jane, John, Brandon Smith]

        //Removes all elements
        //hashMapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition); //returns {}










    }
}
