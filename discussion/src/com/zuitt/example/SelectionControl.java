package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args){
        //Java Operators
        // Arithmetic - +, - , *, /, %
        // Comparison - >, <, >=, <=, ==, !=
        // Logical -> &&, ||, !
        // Assignment -> =
        //Also addition assignment operator
        //a = a + 1, a += 1

        //[SECTION] Control Structure in Java
        // statement allows us to manipulate the flow of the code depending on the evaluation of the condition
        // Syntax:
                /*
                    if(condition){
                        //code block
                    }else{
                        //code block
                    }
                 */

        int num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5.");
        }else{
            System.out.println(num1 + " is not divisible by 5.");
        }
        //36is not divisible by 5.

        //Short Circuiting
        // a technique applicable only to the AND & OR operators wherein if-statements or other control structures can exit early by ensuring either safety of operation or efficiency.
        // right hand operand is not evaluated
        // OR operator
            // (true || ...) = true
        //AND operator
            // (false && ...) = false

        //This is helpful to prevent runtime errors

//        int x = 15;
//        int y = 0;
//        if(x/y == 0){
//            System.out.println("Result is: " + x/y); /// by zero
//        }

        int x = 15;
        int y = 0;
        //no need to evaluate of the second operand
        // in JS it will check the second statement/all
        //it will not waste its time to evaluate the second statement if it is already false
        if(y!= 0 && x/y == 0){
            System.out.println("Result is: " + x/y); /// by zero
        }else{
            System.out.println("This will only run because of short circuiting");
        }

        //Ternary Operator

        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
        System.out.println(result);

        //Switch Cases
        // are control flow structures that allow one code block out of many other code blocks.

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int directionValue = numberScanner.nextInt();

        switch(directionValue){
            //A case block within a switch statement. This represents a single case, or a single possible value for the statement
            case 1:
                System.out.println("North");
                //The break keyword tells that this specific case block has finished.
                //If there is no proper break statement in a case, the code will bleed over to the next case
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default: //the default block handles the scenario if there are no cases that were satisfied
                System.out.println("Invalid");
        }


    }
}
